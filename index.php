<!DOCTYPE html>
<html lang="et">
<head>
    <meta charset="utf-8">
    <title>TODO rakendus</title>
</head>
<body>

<form method="post">
    <input type="text" name="todoItem"/>
    <input type="submit" value="Lisa"/>
</form>

<?php
require "todoList.php";

if (isset($_POST["todoItem"])) {
    add_todo_item($_POST["todoItem"]);
}

if (isset($_POST["finishedItem"])) {
    delete_todo_item($_POST["finishedItem"]);
}

$todoItems = get_todo_items();
?>

<ul>
    <?php foreach ($todoItems as $todoItem) : ?>
        <form method="post">
            <li>
                <?php echo $todoItem ?>
                <input type="hidden" name="finishedItem" value="<?= $todoItem ?>">
                <input type="submit" value="X"/>
            </li>
        </form>

    <?php endforeach; ?>
</ul>

</body>
</html>